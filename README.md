# htxs_xaod

Instruction to run locally (on lxplus).

Setup of AthDerivation:

    setupATLAS
    asetup 21.2,AthDerivation,latest,slc6
    
Download this repository:

    mkdir htxs_aod
    cd htxs_aod
    lsetup git
    git clone ssh://git@gitlab.cern.ch:7999/turra/htxs_xaod.git

Compile:

    mkdir build
    cd build
    cmake ../htxs_xaod
    make
    source x86_64-centos7-gcc62-opt/setup.sh  # this may be different
    cd ..

Run:

    # probably you have to modify the jo, at least for the input file in htxs_aod/MyAnalysis/share/ATestRun_jobOptions.py
    athena MyAnalysis/ATestRun_jobOptions.py
