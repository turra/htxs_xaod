from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

athenaCommonFlags.FilesInput = ["/afs/cern.ch/user/t/turra/work/mc16_13TeV.346214.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam.recon.AOD.e6970_s3126_r9364/AOD.16621923._000436.pool.root.1"]
testFiles = athenaCommonFlags.FilesInput()


import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AppMgr import ServiceMgr
ServiceMgr.EventSelector.InputCollections = testFiles

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()


#from xAODCoreCnv.xAODCoreCnvConf import xAODMaker__AuxStoreWrapper
#alg = xAODMaker__AuxStoreWrapper()
#alg.SGKeys = []
#alg.OutputLevel = DEBUG
#theJob += alg


from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ('MyxAODAnalysis', 'AnalysisAlg')


from DerivationFrameworkHiggs.DerivationFrameworkHiggsConf import DerivationFramework__TruthCategoriesDecorator
HTXSdecorator = DerivationFramework__TruthCategoriesDecorator(name="HTXSdecorator")
ToolSvc += HTXSdecorator

alg.TruthCategoriesDecorator = HTXSdecorator
athAlgSeq += alg
#svcServiceMgr.EventSelector.PrintPerfStats = True 

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
xaodStream = MSMgr.NewPoolRootStream( "StreamAOD", "xAOD.out.root" )
xaodStream.AddItem("xAOD::EventInfo#EventInfo")

htxs_variables = [
    "HTXS_Stage1_2_Category_pTjet25",
    "HTXS_Stage1_2_Category_pTjet30",
    "HTXS_Stage1_2_FineIndex_pTjet30",
    "HTXS_Stage1_2_Fine_Category_pTjet30",
    "HTXS_Stage1_2_Fine_FineIndex_pTjet30",
    "HTXS_isZ2vvDecay",
    "HTXS_errorCode",
    "HTXS_prodMode"]


xaodStream.AddItem("xAOD::EventAuxInfo#EventInfoAux.mcChannelNumber."
                    + '.'.join(htxs_variables))

#xaodStream.AddMetaDataItem( [ "xAOD::TruthMetaDataContainer#TruthMetaData", "xAOD::TruthMetaDataAuxContainer#TruthMetaDataAux." ] )

theApp.EvtMax = 10

include("AthAnalysisBaseComps/SuppressLogging.py")

