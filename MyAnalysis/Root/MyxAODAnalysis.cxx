#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>


MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
      m_tool("TruthCategoriesDecorator/TruthCategoriesDecorator", this)

{
  declareProperty("TruthCategoriesDecorator", m_tool, "The decorator tool");
}


StatusCode MyxAODAnalysis :: initialize ()
{
  ATH_CHECK(m_tool.retrieve());
  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: execute ()
{
  ANA_CHECK(m_tool->addBranches());
  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: finalize ()
{
  return StatusCode::SUCCESS;
}
